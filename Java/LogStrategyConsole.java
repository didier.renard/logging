
public class LogStrategyConsole implements ILogStrategy
{
    @Override
    public void run(ILogInfo info)
    {
        if (info != null)
        {
            System.out.println(info);
        }
    }
}

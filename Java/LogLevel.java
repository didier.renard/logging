
public enum LogLevel
{
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    SUCCESS
}

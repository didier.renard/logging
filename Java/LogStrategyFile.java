import java.io.FileWriter;
import java.io.IOException;

public class LogStrategyFile implements ILogStrategy
{
    public LogStrategyFile(String file)
    {
        this.file = file;
    }

    @Override
    public void run(ILogInfo info)
    {
        if (info != null && this.file != null)
        {
            try
            {
                FileWriter fw = new FileWriter(this.file, true);
                fw.write(info.toString() + "\n");
                fw.close();
            }
            catch(IOException e)
            {
            }
        }
    }

    private String file;
}

/*

LogStrategyFile
        .file

*/

import java.util.HashMap;
import java.util.Arrays;

public class LogInfo implements ILogInfo
{
    @Override
    public void set(String key, Object value)
    {
        if (this.validateKey(key) && value != null)
        {
            this.details.put(key, value);
        }
    }

    @Override
    public Object get(String key)
    {
        if (this.validateKey(key))
        {
            return this.details.get(key);
        }

        return null;
    }

    private String appendIfNotNull(String appendTo, String key)
    {
        if (appendTo != null && key != null)
        {
            Object value = this.get(key);
            if (value != null)
            {
                appendTo += value + " ";
            }
        }

        return appendTo;
    }

    @Override
    public String toString()
    {
        String converted = "";

        for (String key : Arrays.asList(LogInfoKeys.LOG_LEVEL, LogInfoKeys.MESSAGE))
        {
            converted = this.appendIfNotNull(converted, key);
        }

        return converted;
    }

    private boolean validateKey(String key)
    {
        return key != null && key.length() > 0;
    }

    private HashMap<String, Object> details = new HashMap<>();
}

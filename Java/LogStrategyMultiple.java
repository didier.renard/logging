import java.util.List;

public class LogStrategyMultiple implements ILogStrategy
{
    public LogStrategyMultiple(List<ILogStrategy> strategies)
    {
        this.strategies = strategies;
    }

    @Override
    public void run(ILogInfo info)
    {
        for (ILogStrategy strategy : this.strategies)
        {
            strategy.run(info);
        }
    }

    private List<ILogStrategy> strategies;
}


public interface ILogStrategy
{
    void run(ILogInfo info);
}

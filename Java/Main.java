import java.util.Arrays;

class App
{
    public App(ILogger logger)
    {
        this.logger = logger;
    }

    public ILogger logger()
    {
        return this.logger;
    }

    public boolean run(String[] args)
    {
        this.logger().log(LogLevel.DEBUG, "About to run the application, will display arguments (" + args.length + "):");
        for (String arg : args)
        {
            this.logger().log(LogLevel.INFO, arg);
        }
        this.logger().log(LogLevel.DEBUG, "Arguments have been printed out");
        return true;
    }

    private ILogger logger;
}


public class Main
{
    public static void main(String[] args)
    {
        ILogger logger = new Logger(new LogStrategyMultiple(Arrays.asList(new LogStrategyConsole(), new LogStrategyFile("aca.log"))));
        new App(logger).run(args);
    }
}

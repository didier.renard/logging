
public interface ILogger
{
    void log(LogLevel level, String message);
    void log(ILogInfo info);

    ILogInfo createLogInfo();
}

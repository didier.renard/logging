
public class Logger implements ILogger
{
    public Logger(ILogStrategy strategy)
    {
        this.strategy = strategy;
    }

    @Override
    public void log(LogLevel level, String message)
    {
        ILogInfo info = this.createLogInfo();
        info.set(LogInfoKeys.LOG_LEVEL, level);
        info.set(LogInfoKeys.MESSAGE, message);
        this.log(info);
    }

    @Override
    public void log(ILogInfo info)
    {
        this.strategy.run(info);
    }

    @Override
    public ILogInfo createLogInfo()
    {
        return new LogInfo();
    }

    public void debug(String message)
    {
        this.log(LogLevel.DEBUG, message);
    }

    public void info(String message)
    {
        this.log(LogLevel.INFO, message);
    }

    public void warning(String message)
    {
        this.log(LogLevel.WARNING, message);
    }

    public void error(String message)
    {
        this.log(LogLevel.ERROR, message);
    }

    public void success(String message)
    {
        this.log(LogLevel.SUCCESS, message);
    }

    public void setStrategy(ILogStrategy strategy)
    {
        this.strategy = strategy;
    }

    private ILogStrategy strategy;
}

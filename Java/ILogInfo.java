
public interface ILogInfo
{
    void set(String key, Object value);
    Object get(String key);
}

from log_strategy import ILogStrategy
from typing import List


class LogStrategyMultiple(ILogStrategy):
    def __init__(self, strategies: List[ILogStrategy]):
        ILogStrategy.__init__(self)
        self._strategies = strategies

    def run(self, info: 'ILogInfo'):
        for strategy in self._strategies:
            strategy.run(info)

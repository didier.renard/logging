from logger_interface import ILogger
from log_strategy_multiple import LogStrategyMultiple
from log_strategy_console import LogStrategyConsole
from log_strategy_file import LogStrategyFile
from logger import Logger
from log_level import LogLevel
from typing import List
from sys import argv


class App():
    def __init__(self, logger: ILogger):
        self._logger = logger

    def logger(self) -> ILogger:
        return self._logger

    def run(self, args: List[str]) -> bool:
        self.logger().log(LogLevel.DEBUG, f'About to run the application, will display arguments ({len(args)}):')
        for arg in args:
            self.logger().log(LogLevel.INFO, arg)
        self.logger().log(LogLevel.DEBUG, 'Arguments have been printed out')
        return True


if __name__ == '__main__':
    logger = Logger(LogStrategyMultiple([ LogStrategyConsole(), LogStrategyFile('aca.log') ]))
    App(logger).run(argv)

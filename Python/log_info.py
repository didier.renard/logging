from log_info_interface import ILogInfo
from log_info_keys import LogInfoKeys


class LogInfo(ILogInfo):
    def __init__(self):
        ILogInfo.__init__(self)
        self._details = dict()

    def set_value(self, k: str, value: object):
        if self._validate_key(k) and value is not None:
            self._details[k] = value

    def get(self, k: str) -> object:
        if self._validate_key(k) and k in self._details:
            return self._details[k]
        return None

    def _validate_key(self, k: str):
        return k is not None and len(k) > 0

    def _append_if_not_none(self, append_to: str, k: str) -> str:
        if append_to is not None and k is not None:
            value = self.get(k)
            if value is not None:
                append_to += str(value) + ' '
        return append_to

    def __str__(self):
        converted = ''
        for k in [ LogInfoKeys.LOG_LEVEL, LogInfoKeys.MESSAGE ]:
            converted = self._append_if_not_none(converted, k)
        return converted

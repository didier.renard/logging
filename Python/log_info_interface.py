
class ILogInfo():
    def set_value(self, k: str, value: object):
        raise NotImplementedError()

    def get(self, k: str) -> object:
        raise NotImplementedError()


class ILogger():
    def log(self, level: int, message: str):
        raise NotImplementedError()

    def log_info(self, info: 'ILogInfo'):
        raise NotImplementedError()

    def create_log_info(self) -> 'ILogInfo':
        raise NotImplementedError()

from log_strategy import ILogStrategy


class LogStrategyFile(ILogStrategy):
    def __init__(self, file: str):
        ILogStrategy.__init__(self)
        self._file = file

    def run(self, info: 'ILogInfo'):
        if info is not None and self._file is not None:
            with open(self._file, 'a') as f:
                f.write(str(info) + '\n')
                f.close()

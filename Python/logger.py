from logger_interface import ILogger
from log_strategy import ILogStrategy
from log_level import LogLevel
from log_info_interface import ILogInfo
from log_info_keys import LogInfoKeys
from log_info import LogInfo


class Logger(ILogger):
    def __init__(self, strategy: ILogStrategy):
        ILogger.__init__(self)
        self._strategy = strategy

    def log(self, level: LogLevel, message: str):
        info = self.create_log_info()
        info.set_value(LogInfoKeys.LOG_LEVEL, level)
        info.set_value(LogInfoKeys.MESSAGE, message)
        self.log_info(info)

    def log_info(self, info: ILogInfo):
        self._strategy.run(info)

    def create_log_info(self) -> ILogInfo:
        return LogInfo()

    def debug(self, message: str):
        self.log(LogLevel.DEBUG, message)

    def info(self, message: str):
        self.log(LogLevel.INFO, message)

    def warning(self, message: str):
        self.log(LogLevel.WARNING, message)

    def error(self, message: str):
        self.log(LogLevel.ERROR, message)

    def success(self, message: str):
        self.log(LogLevel.SUCCES, message)

    def set_strategy(self, strategy: ILogStrategy):
        self._strategy = strategy

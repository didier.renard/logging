
class ILogStrategy():
    def run(self, info: 'ILogInfo'):
        raise NotImplementedError()

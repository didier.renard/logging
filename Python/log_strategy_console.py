from log_strategy import ILogStrategy


class LogStrategyConsole(ILogStrategy):
    def run(self, info: 'ILogInfo'):
        if info is not None:
            print(str(info))
